const TRANSLATIONS = {
    repos: {
        
    },
    categories: {
        games: {
            de:'Spiele',
            en:'Games'
        },
        communication: {
            de:'Kommunikation',
            en:'Communication'
        },
        entertainment: {
            de: 'Unterhaltung',
            en: 'Entertainment'
        },
        tools: {
            de:'Werkzeuge',
            en:'Tools'
        },
        office: {
            de:'Bürokram',
            en:'Office'
        },
        all: {
            de:'Alle',
            en:'all'
        }
    },
    app: {
        install: {
            de:'Installieren',
            en:'Install'
        },
        open: {
            de:'Öffnen',
            en:'Open'
        }
    },
    menu: {
        add: {
            de:'Hinzufügen',
            en:'Add'
        },
        options: {
            de:'Einstellungen',
            en:'Options'
        },
        tide: {
            de:'TIDE',
            en:'TIDE'
        },
        search: {
            de:'Suche',
            en:'Search'
        }
    }
};
export {
    TRANSLATIONS
}
